# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone
git rm nepotrebno.js
git add jscolor.js
git commit -m "Priprava potrebnih JavaScript knjižnic"
git push
```

Naloga 6.2.3:
https://bitbucket.org/xNikey/stroboskop/commits/c227886550c66086a76d8e983276efcc35147ecd

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/xNikey/stroboskop/commits/180407f215844249137d1d26d2c75cb618b2d6de

Naloga 6.3.2:
https://bitbucket.org/xNikey/stroboskop/commits/a12a27d86d179959ca73ebaa8e759391c84db26f

Naloga 6.3.3:
https://bitbucket.org/xNikey/stroboskop/commits/0123674d25bf38fd0dca34980c426ae55ab1ce78

Naloga 6.3.4:
https://bitbucket.org/xNikey/stroboskop/commits/e9f12b878f5a0255f023822d2ddab85dc5281f6a

Naloga 6.3.5:

```
git branch izgled
git checkout izgled
git commit -a
git push
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/xNikey/stroboskop/commits/7f51edaeed2ac7dec6bfd59e5fdb179714739d42

Naloga 6.4.2:
https://bitbucket.org/xNikey/stroboskop/commits/19b5a825c674dd33d886e43043cb656ddfd154c0

Naloga 6.4.3:
https://bitbucket.org/xNikey/stroboskop/commits/4529949ddfe922807cc5697ac4b81eefdfbee7eb

Naloga 6.4.4:
https://bitbucket.org/xNikey/stroboskop/commits/6fabce634c4deaf188ce6451e0f5d4544719b5a9